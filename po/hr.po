# Croatian translation for read-it-later.
# Copyright (C) 2022 read-it-later's COPYRIGHT HOLDER
# This file is distributed under the same license as the read-it-later package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: read-it-later master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/read-it-later/issues\n"
"POT-Creation-Date: 2022-10-30 12:58+0000\n"
"PO-Revision-Date: 2023-01-13 13:06+0100\n"
"Last-Translator: \n"
"Language-Team: Croatian <hr@li.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.2.2\n"

#: data/com.belmoussaoui.ReadItLater.desktop.in.in:3
#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:65
msgid "Read It Later"
msgstr "Pročitaj kasnije"

#: data/com.belmoussaoui.ReadItLater.desktop.in.in:4
msgid "A Wallabag Client"
msgstr "Wallabag klijent"

#: data/com.belmoussaoui.ReadItLater.desktop.in.in:9
msgid "Gnome;GTK;Wallabag;Web;Article;Offline;"
msgstr "Gnome;GTK;Wallabag;Web;Članak;Izvanmrežno;"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:8
msgid "A Wallabag Client."
msgstr "Wallabag klijent."

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:10
msgid ""
"Read It Later, is a simple Wallabag client. It has the basic features to "
"manage your articles"
msgstr ""
"Pročitaj kasnije, je jednostavan Wallabag klijent. Ima osnovne značajke za "
"upravljanje vašim člancima"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:12
msgid "Add new articles"
msgstr "Dodaj nove članke"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:13
msgid "Archive an article"
msgstr "Arhiviraj članak"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:14
msgid "Delete an article"
msgstr "Obriši članak"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:15
msgid "Favorite an article"
msgstr "Dodaj članak u omiljene"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:17
msgid ""
"It also comes with a nice on eyes reader mode that supports code syntax "
"highlighting and a dark mode"
msgstr ""
"Dolazi s ugodnim načinom čitanja koji podržava isticanje sintakse kôda i "
"tamni način rada"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:22
msgid "Main Window"
msgstr "Glavni prozor"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:26
msgid "Article View"
msgstr "Prikaz članka"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:30
msgid "Adaptive view in dark mode"
msgstr "Prilagodljivi prikaz u tamnom načinu rada"

#: data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in:58
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/com.belmoussaoui.ReadItLater.gschema.xml.in:6
msgid "Default window width"
msgstr "Zadana širina prozora"

#: data/com.belmoussaoui.ReadItLater.gschema.xml.in:10
msgid "Default window height"
msgstr "Zadana visina prozora"

#: data/com.belmoussaoui.ReadItLater.gschema.xml.in:14
msgid "Default window x position"
msgstr "Zadani x položaj prozora"

#: data/com.belmoussaoui.ReadItLater.gschema.xml.in:18
msgid "Default window y position"
msgstr "Zadani y položaj prozora"

#: data/com.belmoussaoui.ReadItLater.gschema.xml.in:22
msgid "Default window maximized behavior"
msgstr "Zadano ponašanje uvećanja prozora"

#: data/com.belmoussaoui.ReadItLater.gschema.xml.in:26
msgid "Logged in user name"
msgstr "Korisničko ime prijave"

#: data/com.belmoussaoui.ReadItLater.gschema.xml.in:30
msgid "Latest successful sync in ms"
msgstr "Posljednje uspješno usklađivanje u ms"

#: data/resources/ui/articles_list.ui:34
msgid "Pretty clean!"
msgstr "Ljepši prikaz!"

#: data/resources/ui/gtk/help-overlay.ui:12
msgctxt "shortcut window"
msgid "General"
msgstr "Općenito"

#: data/resources/ui/gtk/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Prikaži prečace"

#: data/resources/ui/gtk/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Preferences"
msgstr "Osobitosti"

#: data/resources/ui/gtk/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Back"
msgstr "Natrag"

#: data/resources/ui/gtk/help-overlay.ui:33
msgctxt "shortcut window"
msgid "Quit"
msgstr "Zatvori"

#: data/resources/ui/gtk/help-overlay.ui:41
msgctxt "shortcut window"
msgid "Articles"
msgstr "Članci"

#: data/resources/ui/gtk/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Synchronize"
msgstr "Uskladi"

#: data/resources/ui/gtk/help-overlay.ui:50
msgctxt "shortcut window"
msgid "New Article"
msgstr "Novi članak"

#: data/resources/ui/gtk/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Delete Article"
msgstr "Obriši članak"

#: data/resources/ui/gtk/help-overlay.ui:62
msgctxt "shortcut window"
msgid "Favorite Article"
msgstr "Omiljeni članak"

#: data/resources/ui/gtk/help-overlay.ui:68
msgctxt "shortcut window"
msgid "Archive Article"
msgstr "Arhiviraj članak"

#: data/resources/ui/gtk/help-overlay.ui:74
msgctxt "shortcut window"
msgid "Open Article in Browser"
msgstr "Otvori članak u pregledniku"

#: data/resources/ui/login.ui:32
msgid "Welcome to Read It Later"
msgstr "Dobrodošli u Pročitaj kasnije"

#: data/resources/ui/login.ui:42
msgid "Please sign in using your Wallabag instance information"
msgstr "Prijavite se pomoću vaših Wallabag podataka"

#: data/resources/ui/login.ui:57
msgid "Instance URL"
msgstr "URL primjeraka"

#: data/resources/ui/login.ui:63
msgid "Client ID"
msgstr "ID klijenta"

#: data/resources/ui/login.ui:69
msgid "Client Secret"
msgstr "Tajna klijenta"

#: data/resources/ui/login.ui:75 data/resources/ui/settings.ui:13
msgid "User Name"
msgstr "Korisničko ime"

#: data/resources/ui/login.ui:81
msgid "Password"
msgstr "Lozinka"

#: data/resources/ui/login.ui:91
msgid "Log In"
msgstr "Prijava"

#: data/resources/ui/settings.ui:10
msgid "Account Information"
msgstr "Informacije računa"

#: data/resources/ui/settings.ui:21
msgid "Email"
msgstr "E-pošta"

#: data/resources/ui/settings.ui:29
msgid "Created At"
msgstr "Stvoreno"

#: data/resources/ui/settings.ui:37
msgid "Updated At"
msgstr "Nadopunjeo"

#: data/resources/ui/window.ui:8
msgid "Open Website"
msgstr "Otvori web stranicu"

#: data/resources/ui/window.ui:14
msgid "Delete Article"
msgstr "Obriši članak"

#: data/resources/ui/window.ui:22
msgid "Add Article…"
msgstr "Dodaj članak…"

#: data/resources/ui/window.ui:28
msgid "Preferences"
msgstr "Osobitosti"

#: data/resources/ui/window.ui:32
msgid "Log Out"
msgstr "Odjava"

#: data/resources/ui/window.ui:38
msgid "Keyboard Shortcuts"
msgstr "Prečaci tipkovnice"

#: data/resources/ui/window.ui:42
msgid "About Read It Later"
msgstr "O Pročitaj kasnije"

#: data/resources/ui/window.ui:103
msgid "Favorite"
msgstr "Omiljeno"

#: data/resources/ui/window.ui:111
msgid "Archive"
msgstr "Arhiva"

#: data/resources/ui/window.ui:150
msgid "Paste the article URL here"
msgstr "Zalijepi URL članka ovdje"

#: data/resources/ui/window.ui:157
msgid "Save"
msgstr "Spremi"

#: src/application.rs:108
msgid "Failed to logout"
msgstr "Neuspjela odjava"

#: src/application.rs:252 src/application.rs:257
msgid "Failed to log in"
msgstr "Neuspjela prijava"

#: src/application.rs:401
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  gogo https://launchpad.net/~trebelnik-stefina"

#~ msgid "No articles found"
#~ msgstr "Nema pronađenih članaka"

#~ msgid "Logout"
#~ msgstr "Odjava"
